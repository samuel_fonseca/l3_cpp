#ifndef TRANSPORT_SRC_LIST_HPP
#define TRANSPORT_SRC_LIST_HPP

#include "cell.hpp"
#include "train.hpp"

#include <memory>

struct Cell;
struct Train;

struct List
{
  Cell *first;
  Cell *last;

  List();
  List(const List &other);
  void add_first(Train *train);
  void add_last(Train *train);
  void add_last(const std::shared_ptr<Train>& train);
  void append(List list);
  unsigned int size();
  void remove_first();
  void remove_last();
  ~List();
};

#endif //TRANSPORT_SRC_LIST_HPP
