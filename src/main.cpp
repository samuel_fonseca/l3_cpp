#include "kernel.hpp"
#include "iterator.hpp"
#include "line.hpp"

#include <iostream>

int main()
{
  Line *line_1 = new Line("L1", 6, 8, 4);
  Line *line_2 = new Line("L2", 5, 6, 6);
  Kernel kernel(2);

  line_1->add_station("1", 2, 4.1f);
  line_1->add_station("2_1", 2, 3.5f);
  line_1->add_station("3", 2, 4.2f);
  line_1->add_station("4", 2, 3.2f);
  line_1->add_station("5_1", 2, 5.f);
  line_1->add_station("6", 2);
  line_1->build_trains();

  line_2->add_station("7", 2, 7.2f);
  line_2->add_station("8", 2, 5.1f);
  line_2->add_station("2_2", 2, 2.2f);
  line_2->add_station("9", 2, 2.7f);
  line_2->add_station("10", 2);
  line_2->build_trains();

  kernel.add_line(line_1);
  kernel.add_line(line_2);

  unsigned int time = 0;

  while (time < 4000) {
    unsigned int current_time = time;

    time = kernel.run(time);

    List stopped_trains = kernel.get_trains(current_time, STOP);
    Iterator it(stopped_trains, true);

    while (it.has_more()) {
      std::cout << current_time << ": " << it.current()->id
                << " in station " << it.current()->line.stations[it.current()->station_index].name
                << " of line " << it.current()->line.name
                << std::endl;
      it.next();
    }
  }
  return 0;
}