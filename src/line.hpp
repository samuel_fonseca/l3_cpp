#ifndef TRANSPORT__LINE_HPP
#define TRANSPORT__LINE_HPP

#include "list.hpp"
#include "station.hpp"

#include <valarray>

struct List;

struct Line
{
  char *name;
  unsigned int station_number;
  std::valarray<unsigned int> durations;
  Station *stations;
  unsigned int flip_duration;
  unsigned int station_index;
  unsigned int train_number;
  List trains;
  unsigned int next_time;

  Line(const char *name, unsigned int station_number, unsigned int train_number, float flip_duration);
  void add_station(const char *name, float stop_duration, float duration = 0);
  void build_trains();
  unsigned int get_total_duration();
  List get_trains(unsigned int time, State state);
  unsigned int run(unsigned int time);
  ~Line();
};

//typedef Line *LinePtr;
typedef std::unique_ptr<Line> LinePtr;

#endif //TRANSPORT__LINE_HPP
