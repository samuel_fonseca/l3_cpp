#include "list.hpp"

#include <iostream>

List::List() : first(nullptr), last(nullptr)
{

//  std::cout << "[List] - constructor" << std::endl;

}

List::List(const List &other) : first(nullptr), last(nullptr)
{
  Cell *current = other.first;

  while (current != nullptr) {
    add_last(current->train);
    current = current->next;
  }

//  std::cout << "[List] - copy constructor" << std::endl;

}

void List::add_first(Train *train)
{
  Cell* new_cell = new Cell(train, nullptr, first);

  if (first != nullptr) {
    first->previous = new_cell;
  }
  first = new_cell;
  last = last == nullptr ? first : last;
}

void List::add_last(Train *train)
{
  Cell* new_cell = new Cell(train, last, nullptr);

  if (last != nullptr) {
    last->next = new_cell;
  }
  last = new_cell;
  first = first == nullptr ? last : first;
}

void List::add_last(const std::shared_ptr<Train>& train)
{
  Cell* new_cell = new Cell(train, last, nullptr);

  if (last != nullptr) {
    last->next = new_cell;
  }
  last = new_cell;
  first = first == nullptr ? last : first;
}

void List::append(List list)
{
  Cell *current = list.first;

  while (current != nullptr) {
    add_last(current->train);
    current = current->next;
  }
}

unsigned int List::size()
{
  unsigned int size = 0;
  Cell *current = first;

  while (current != last) {
    ++size;
    current = current->next;
  }
  return size;
}

void List::remove_first()
{
  if (first != nullptr) {
    Cell *removed_cell = first;

    first = first->next;
    if (first == nullptr) {
      last = nullptr;
    }
    delete removed_cell;
  }
}

void List::remove_last()
{
  if (last != nullptr) {
    Cell *removed_cell = last;

    last = last->previous;
    if (last == nullptr) {
      first = nullptr;
    }
    delete removed_cell;
  }
}

List::~List()
{
  while (first != nullptr) {
    Cell *next = first->next;

    delete first;
    first = next;
  }

//  std::cout << "[List] - destructor" << std::endl;

}

