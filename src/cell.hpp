#ifndef TRANSPORT_SRC_CELL_HPP
#define TRANSPORT_SRC_CELL_HPP

#include "train.hpp"

#include <memory>

struct Cell
{
  std::shared_ptr<Train> train;
  Cell *next;
  Cell *previous;

  Cell(Train *train, Cell *previous, Cell *next);
  Cell(const std::shared_ptr<Train> &train, Cell *previous, Cell *next);
  ~Cell();
};

#endif //TRANSPORT_SRC_CELL_HPP
