#ifndef TRANSPORT__KERNEL_HPP
#define TRANSPORT__KERNEL_HPP

#include "line.hpp"
#include "train.hpp"

struct Kernel
{
  LinePtr *lines;
  unsigned int line_number;
  unsigned int line_index;

  Kernel(unsigned int line_number);
  void add_line(Line *line);
  List get_trains(unsigned int time, State state);
  unsigned int run(unsigned int time);
  ~Kernel();
};

#endif //TRANSPORT__KERNEL_HPP
