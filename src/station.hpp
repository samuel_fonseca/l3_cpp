#ifndef TRANSPORT__STATION_HPP
#define TRANSPORT__STATION_HPP

struct Station
{
  char *name;
  unsigned int stop_duration;

  Station() : name(nullptr)
  {}
};

typedef Station *StationPtr;

#endif //TRANSPORT__STATION_HPP
