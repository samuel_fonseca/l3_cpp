#include "iterator.hpp"
#include "line.hpp"
#include "list.hpp"

#include <climits>
#include <cstring>
#include <iostream>

Line::Line(const char *name,
           unsigned int station_number,
           unsigned int train_number,
           float flip_duration) : stations(nullptr), durations(station_number - 1)
{
  this->name = new char[strlen(name) + 1];
  strcpy(this->name, name);
  this->station_number = station_number;
  stations = new Station[station_number];
  this->train_number = train_number;
  this->flip_duration = (int) (flip_duration * 60);
  station_index = 0;
  next_time = INT_MAX;

  std::cout << "[Line] - constructor" << std::endl;

}

void Line::add_station(const char *name, float stop_duration, float duration)
{
  stations[station_index].name = new char[strlen(name) + 1];
  strcpy(stations[station_index].name, name);
  stations[station_index].stop_duration = (int) (stop_duration * 60);
  if (station_index < station_number - 1) {
    durations[station_index] = (int) (duration * 60);
  }
  station_index++;
}

void Line::build_trains()
{
  for (unsigned int i = 0; i < train_number; ++i) {
    trains.add_last(new Train(*this, 0, i, UP));
  }
}

unsigned int Line::get_total_duration()
{
  unsigned int total = 0;

  for (unsigned int i = 0; i < station_number; ++i) {
    total += stations[i].stop_duration;
    if (i < station_number - 1) {
      total += durations[i];
    }
  }
  total += 2 * flip_duration;
  return total;
}

List Line::get_trains(unsigned int time, State state)
{
  List list;
  Iterator it(trains, true);

  while (it.has_more()) {
    if (it.current()->state == state and it.current()->last_time == time) {
      list.add_last(it.current());
    }
    it.next();
  }
  return list;
}

unsigned int Line::run(unsigned int time)
{
  unsigned int min = INT_MAX;
  Iterator it(trains, true);

  while (it.has_more()) {
    if (it.current()->next_time == time) {
      it.current()->run(time);
    }
    if (it.current()->next_time < min) {
      min = it.current()->next_time;
      next_time = min;
    }
    it.next();
  }
  return next_time;
}

Line::~Line()
{
  delete[] name;
  for (unsigned int i = 0; i < station_number; ++i) {
    delete[] stations[i].name;
  }
  delete[] stations;

  std::cout << "[Line] - destructor" << std::endl;

}