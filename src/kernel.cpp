#include "kernel.hpp"

#include <climits>
#include <cstring>
#include <iostream>

Kernel::Kernel(unsigned int line_number) : lines(nullptr), line_number(line_number), line_index(0)
{
  lines = new LinePtr[line_number];

  std::cout << "[Kernel] - constructor" << std::endl;

}

void Kernel::add_line(Line *line)
{
  lines[line_index] = std::unique_ptr<Line>(line);
  ++line_index;
}

List Kernel::get_trains(unsigned int time, State state)
{
  List list;

  for (unsigned int i = 0; i < line_number; ++i) {
    List line_list = lines[i]->get_trains(time, state);

    list.append(line_list);
  }
  return list;
}

unsigned int Kernel::run(unsigned int time)
{
  unsigned int min = INT_MAX;

  for (unsigned int i = 0; i < line_number; ++i) {
    unsigned int min_next_time = lines[i]->run(time);

    if (min_next_time < min) {
      min = min_next_time;
    }
  }
  return min;
}

Kernel::~Kernel()
{
//  for (unsigned int i = 0; i < line_number; ++i) {
//    delete lines[i];
//  }
  delete[] lines;

  std::cout << "[Kernel] - destructor" << std::endl;

}