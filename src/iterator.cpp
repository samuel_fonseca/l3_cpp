#include <iostream>
#include "iterator.hpp"

Iterator::Iterator(List &list, bool forward)
    : list(list), current_cell(list.first), forward(forward)
{

//  std::cout << "[Iterator] - constructor" << std::endl;

}

const std::shared_ptr<Train>& Iterator::current()
{
  return current_cell->train;
}

bool Iterator::has_more()
{
  return current_cell != nullptr;
}

void Iterator::next()
{
  current_cell = forward ? current_cell->next : current_cell->previous;
}