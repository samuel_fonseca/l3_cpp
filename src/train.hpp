#ifndef TRANSPORT__TRAIN_HPP
#define TRANSPORT__TRAIN_HPP

#include "state.hpp"
#include "way.hpp"

struct Line;

struct Train
{
  char *id;
  unsigned int position;
  unsigned int delay;
  Line &line;
  unsigned int station_index;
  State state;
  Way way;
  unsigned int next_time;
  unsigned int last_time;

  Train(Line &line, unsigned int start_time, unsigned int position, Way way);
  Train(const Train& other);
  void display();
  void init(unsigned int time);
  void run(unsigned int time);
  ~Train();
};

typedef struct Train *TrainPtr;

#endif //TRANSPORT__TRAIN_HPP
