#ifndef TRANSPORT_SRC_ITERATOR_HPP
#define TRANSPORT_SRC_ITERATOR_HPP

#include "list.hpp"

#include <memory>

struct Iterator
{
  List& list;
  Cell* current_cell;
  bool forward;

  Iterator(List& list, bool forward);
  const std::shared_ptr<Train>& current();
  bool has_more();
  void next();
};

#endif //TRANSPORT_SRC_ITERATOR_HPP
