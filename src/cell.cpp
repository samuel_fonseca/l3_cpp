#include "cell.hpp"

#include <iostream>

Cell::Cell(Train *train, Cell *previous, Cell *next)
    : train(train), previous(previous), next(next)
{

//  std::cout << "[Cell] - constructor" << std::endl;

}

Cell::Cell(const std::shared_ptr<Train> &train, Cell *previous, Cell *next)
    : train(train), previous(previous), next(next)
{

//  std::cout << "[Cell] - constructor" << std::endl;

}

Cell::~Cell()
{

//  delete train;

//  std::cout << "[Cell] - destructor" << std::endl;

}




