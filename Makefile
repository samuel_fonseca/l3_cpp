main: main.o kernel.o line.o state.o train.o
	g++ -o main main.o kernel.o line.o state.o train.o

kernel.o: src/kernel.cpp src/kernel.hpp
	g++ -o kernel.o -c src/kernel.cpp -W -Wall -pedantic -std=c++14

line.o: src/line.cpp src/line.hpp
	g++ -o line.o -c src/line.cpp -W -Wall -pedantic -std=c++14

state.o: src/state.cpp src/state.hpp
	g++ -o state.o -c src/state.cpp -W -Wall -pedantic -std=c++14

train.o: src/train.cpp src/train.hpp
	g++ -o train.o -c src/train.cpp -W -Wall -pedantic -std=c++14

main.o: src/main.cpp
	g++ -o main.o -c src/main.cpp -W -Wall -pedantic -std=c++14

clean:
	rm -rf *.o main